package com.example.winie.newfileprovider;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.content.MimeTypeFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MainActivity extends AppCompatActivity {

    String imageFileName = "ApacheSoftwareFoundationLogo2016.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try (InputStream inputFileStream = getResources().getAssets().open(imageFileName);
             OutputStream appStorageOutputStream = this.openFileOutput(imageFileName, Context.MODE_PRIVATE);
        ) {
            IOUtils.copy(inputFileStream, appStorageOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        File file = new File(getApplicationContext().getFilesDir(), imageFileName);

        ImageView imageView = (ImageView) findViewById(R.id.image_view);
        imageView.setImageDrawable(Drawable.createFromPath(file.getPath()));

        try {
            String[] files = {file.getAbsolutePath().toString()};
            String[] mimeTypes = {Files.probeContentType(file.toPath())};
            MediaScannerConnection.scanFile(this, files, mimeTypes, null);
        } catch (Exception e) {
            e.printStackTrace();
        }


        this.requireViewById(R.id.show_image_button).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        File file = new File(getApplicationContext().getFilesDir(), imageFileName);
                        if (file.exists()) {
                            try {
                                final Intent intent = new Intent(Intent.ACTION_DEFAULT);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                String mimeType = Files.probeContentType(file.toPath());
                                intent.setData(Uri.fromFile(file));
                                Uri fileUri = FileProvider.getUriForFile(
                                        MainActivity.this.getApplicationContext(),
                                        MainActivity.this.getApplicationContext().getPackageName() + ".fileprovider",
                                        file);
                                intent.setDataAndType(fileUri, mimeType);
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                MainActivity.this.getApplicationContext().startActivity(intent);
                            } catch (IOException exception) {
                                exception.printStackTrace();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Datei \"" + file.toString() + "\" existiert nicht", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
        );
    }
}
